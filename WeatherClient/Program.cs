﻿using System;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Collections.Generic;

namespace WeatherClient
{

    /* structure of town */
    struct Town
    {
        /* id of town in Yahoo for weather */
        public string weatherID;
        /* name of town */
        public string rowTown;
    }

    class YahooClient
    {

        /* constants for Yahoo client */
        const string cURL = "https://weather-ydn-yql.media.yahoo.com/forecastrss";
        const string cAppID = "61aAr97k";
        const string cConsumerKey = "dj0yJmk9ak1NREpES3F3dk1qJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTdh";
        const string cConsumerSecret = "2fd740eeafadc8582058b35bfc47e6ea1a842770";
        const string cOAuthVersion = "1.0";
        const string cOAuthSignMethod = "HMAC-SHA1";
        const string cUnitID = "u=c";
        const string cFormat = "xml";

        /* list of towns for weather */
        public static List<Town> towns = new List<Town>();

        /* days of week for parsing */
        const string Monday = "Mon";
        const string Tuesdey = "Tue";
        const string Wednsday = "Wed";
        const string Thursday = "Thu";
        const string Friday = "Fri";
        const string Saturday = "Sat";
        const string Sunday = "Sun";

        /* label string to find weather */
        const string labelString = "<BR />";

        /* message for greeting */
        const string greetMessage = "If you want to exit program, you shoult write 'exit'\n" +
                                    "If you want to find out weather of some town, you should write 'town <some town>'\n" +
                                    "On other hand, you will recieve error";
       
        /* commands for user */
        const string exitCommand = "exit";
        const string townCommand = "town";

        /* message of errors */
        const string errCommand = "You wrote uncorrect command";
        const string errTown = "This town does not exist in the system";
        const string errTownEx = "You did not write town";

        /* function to show message in terminal */
        public static void showMessage(string message)
        {
            Console.WriteLine(message);
        }

        /* function to get time */
        public static string getTime()
        {

            TimeSpan lTS = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            /* return time as string */
            return Convert.ToInt64(lTS.TotalSeconds).ToString();

        }

        /* function get current time */
        public static string getNonce()
        {

            /* return current time as string */
            return Convert.ToBase64String(
             new ASCIIEncoding().GetBytes(
              DateTime.Now.Ticks.ToString()
             )
            );

        }

        /* function to get authentication string */
        public static string getAuth(string weatherID)
        {

            /* initialization of time parameters */
            string lNonce = getNonce();
            string lTimes = getTime();

            /* initialization of secure key */
            string lCKey = string.Concat(cConsumerSecret, "&");

            /* initialization of user parameters  */
            string lSign = string.Format(
             "format={0}&" +
             "oauth_consumer_key={1}&" +
             "oauth_nonce={2}&" +
             "oauth_signature_method={3}&" +
             "oauth_timestamp={4}&" +
             "oauth_version={5}&" +
             "{6}&{7}",
             cFormat,
             cConsumerKey,
             lNonce,
             cOAuthSignMethod,
             lTimes,
             cOAuthVersion,
             cUnitID,
             weatherID
            );

            /* write query to get information */
            lSign = string.Concat(
             "GET&", Uri.EscapeDataString(cURL), "&", Uri.EscapeDataString(lSign)
            );

            /* encode request */
            using (var lHasher = new HMACSHA1(Encoding.ASCII.GetBytes(lCKey)))
            {
                lSign = Convert.ToBase64String(
                 lHasher.ComputeHash(Encoding.ASCII.GetBytes(lSign))
                );
            }

            /* return authentication string */
            return "OAuth " +
                   "oauth_consumer_key=\"" + cConsumerKey + "\", " +
                   "oauth_nonce=\"" + lNonce + "\", " +
                   "oauth_timestamp=\"" + lTimes + "\", " +
                   "oauth_signature_method=\"" + cOAuthSignMethod + "\", " +
                   "oauth_signature=\"" + lSign + "\", " +
                   "oauth_version=\"" + cOAuthVersion + "\"";

        }

        /* make query and resurn result */
        public static string makeQuery(string weatherID)
        {

            /* make ulr address */
            string lURL = cURL + "?" + weatherID + "&" + cUnitID + "&format=" + cFormat;

            /* create web client */
            WebClient lClt = new WebClient();

            /* make headers for query */
            lClt.Headers.Set("Content-Type", "application/" + cFormat);
            lClt.Headers.Add("X-Yahoo-App-Id", cAppID);
            lClt.Headers.Add("Authorization", getAuth(weatherID));

            /* make query and save result */
            byte[] lDataBuffer = lClt.DownloadData(lURL);
            string lOut = Encoding.ASCII.GetString(lDataBuffer);

            /* create and load Xml-document for parsing */
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(lOut);

            /* parse Xml-file */
            string text = "";
            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                text = node.InnerText;
            }

            /* return result */
            return text;

        }

        /* function to show results */
        public static void showResult(string result)
        {

            /* split result using symbol '\n' */
            string[] rows = result.Split('\n');

            /* go through all rows */
            int i = 0;
            while (i < rows.Length)
            {
                /* find rows with label */
                if (rows[i].Contains(labelString))
                {
                    /* find out weather for day of week */
                    if (rows[i].Contains(Monday))
                    {
                        showMessage(rows[i].Remove(0, 7));
                    }
                    else if (rows[i].Contains(Tuesdey))
                    {
                        showMessage(rows[i].Remove(0, 7));
                    }
                    else if (rows[i].Contains(Wednsday))
                    {
                        showMessage(rows[i].Remove(0, 7));
                    }
                    else if (rows[i].Contains(Thursday))
                    {
                        showMessage(rows[i].Remove(0, 7));
                    }
                    else if (rows[i].Contains(Friday))
                    {
                        showMessage(rows[i].Remove(0, 7));
                    }
                    else if (rows[i].Contains(Saturday))
                    {
                        showMessage(rows[i].Remove(0, 7));
                    }
                    else if (rows[i].Contains(Sunday))
                    {
                        showMessage(rows[i].Remove(0, 7));
                    }
                }
                i++;
            }

        }

        public static int findTown(string town)
        {

            /* initial value */
            int answer = -1;

            int i = 0;
            while (i < towns.Count)
            {
                /* find out town in the list */
                if (town == towns[i].rowTown)
                {
                    answer = i;
                    break;
                }
                i++;
            }

            /* return answer */
            return answer;

        }

        /* function to add new town to the list */
        public static void addNewTown(string weatherID, string rowTown)
        {

            /* and new town */
            Town town = new Town();
            town.weatherID = weatherID;
            town.rowTown = rowTown;
            towns.Add(town);

        }

        /* function to enter towns for list */
        public static void enterTowns()
        {

            /* Cherkasy */
            addNewTown("woeid=918180", "Cherkasy");
            /* Kiev */
            addNewTown("woeid=924938", "Kiev");
            /* Lviv */
            addNewTown("woeid=924943", "Lviv");
            /* Chernivtsi */
            addNewTown("woeid=918247", "Chernivtsi");
            /* Kharkiv */
            addNewTown("922137", "Kharkiv");
            /* Odesa */
            addNewTown("woeid=929398", "Odesa");
            /* Dnipro */
            addNewTown("woeid=918981", "Dnipro");
            /* Poltava */
            addNewTown("woeid=931366", "Poltava");
            /* Rivne */
            addNewTown("woeid=932520", "Rivne");

        }

        public static void Main(string[] args)
        {

            /* enter towns in list */
            enterTowns();

            while (true)
            {

                /* show message */
                showMessage(greetMessage);

                /* receive command from terminal */
                string command = Console.ReadLine();
                string[] substrings = command.Split(' ');

                /* handle command of exiting */
                if (substrings[0] == exitCommand)
                {
                    /* exit program */
                    break;
                }

                /* handle command to find out weather of some town */
                if (substrings[0] == townCommand) 
                {
                    /* check if name of town exists */
                    if (substrings.Length < 2)
                    {
                        /* write error */
                        showMessage(errTownEx);
                    }
                    else
                    {
                        /* find index of town */
                        int index = findTown(substrings[1]);
                        if (index != -1)
                        {
                            /**/
                            string result = makeQuery(towns[index].weatherID);
                            showMessage("Weather in " + substrings[1] + ":");
                            showResult(result);
                        }
                        else
                        {
                            /* write error */
                            showMessage(errTown);
                        }
                    }
                } 
                else
                {
                    /* write error */
                    showMessage(errCommand);
                }

            }

        }

    }

}
